# README #

### What is this repository for? ###

This repository is created for hosting the code of a technical assignment.
 

### How do I get set up? ###

Workflow is as follows, assuming that the jdk8 and maven are appropriately installed.

To compile the entire project, run `mvn clean install`. This will create a war file under the target directory.

To run the application, run `mvn spring-boot:run` and browse to http://localhost:8080/ .

Also, the war file can be deployed to an application server, e.g. tomcat to see the running application.


### How to use the running application? ###

When the application runs it creates two endpoints:

1. /working which returns a page with the text "It works".
2. /rest/working which returns a json object: {"value":"It works"}. 
This JSON value is loaded from a database (the details of the DB can be seen in application.properties file).

 
### Who do I talk to? ###

Repo owner is Cem Kilickaplan.