package com.lukkien.assignment;

import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.lukkien.assignment.controller.ApplicationController;
import com.lukkien.assignment.model.DBResponseEntity;

@RunWith(SpringRunner.class)
@WebMvcTest(ApplicationController.class)
public class AssignmentApplicationTests
{
	@MockBean
	private ApplicationController _applicationController;

	private MockMvc _mockMvc;

	@Autowired
	private WebApplicationContext _webApplicationContext;

	@Before
	public void setUp()
	{
		_mockMvc = MockMvcBuilders.webAppContextSetup(_webApplicationContext).build();
	}

	@Test
	public void testWorking() throws Exception
	{
		_mockMvc.perform(get("/working").content("yes!")).andExpect(status().isOk());
	}

	@Test
	public void testWorkingJson() throws Exception
	{
		DBResponseEntity responseEntity = new DBResponseEntity();
		responseEntity.setValue("{\"value\":\"It works\"}");

		BDDMockito.given(_applicationController.itWorksWithJSONResponse()).willReturn(responseEntity);

		//@Formatter:off
		_mockMvc.perform(get("/rest/working")
			.contentType(APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.value", is(responseEntity.getValue())));
		//@Formatter:on
	}

	@Test
	public void testResourceNotFound() throws Exception
	{
		_mockMvc.perform(get("/test")).andExpect(status().is4xxClientError());
	}
}
