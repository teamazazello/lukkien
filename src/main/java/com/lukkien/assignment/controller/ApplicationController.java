package com.lukkien.assignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.lukkien.assignment.exception.ResourceNotFoundException;
import com.lukkien.assignment.model.DBResponseEntity;
import com.lukkien.assignment.repository.MainRepository;

@RestController
public class ApplicationController
{
	@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
	@Autowired
	MainRepository _mainRepository;

	@RequestMapping(value = "/working", method = RequestMethod.GET)
	public String itWorks()
	{
		return "It Works";
	}

	@RequestMapping(value = "/rest/working", method = RequestMethod.GET)
	public DBResponseEntity itWorksWithJSONResponse()
	{
		final Iterable<DBResponseEntity> entities = _mainRepository.findAll();

		if (entities.iterator().hasNext())
		{
			return entities.iterator().next();
		}
		else
		{
			throw new ResourceNotFoundException();
		}
	}
}
