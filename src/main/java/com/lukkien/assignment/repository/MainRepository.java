package com.lukkien.assignment.repository;

import org.springframework.data.repository.CrudRepository;
import com.lukkien.assignment.model.DBResponseEntity;

public interface MainRepository extends CrudRepository<DBResponseEntity, Long>
{
}
