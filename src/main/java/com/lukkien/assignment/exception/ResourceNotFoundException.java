package com.lukkien.assignment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException
{
	private static final long serialVersionUID = -4931615994183584265L;

	public ResourceNotFoundException()
	{
		super("The requested JSON value was not found in the database.");
	}
}
