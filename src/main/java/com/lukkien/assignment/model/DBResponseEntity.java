package com.lukkien.assignment.model;

import javax.persistence.*;

@Entity
@Table(name = "lukkien")
public class DBResponseEntity
{
	@Id // Not a real ID. This is here to prevent the "No identifier specified" error.
	@Column(name = "value")
	private String _value;

	public String getValue()
	{
		return _value;
	}

	public void setValue(final String value)
	{
		_value = value;
	}
}
